-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : db:3306
-- Généré le : sam. 24 fév. 2024 à 13:53
-- Version du serveur : 8.3.0
-- Version de PHP : 8.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `msi_bank`
--

-- --------------------------------------------------------

--
-- Structure de la table `Agent`
--

CREATE TABLE `Agent` (
  `Id_Agent` int NOT NULL,
  `Id_Employe` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Agent`
--

INSERT INTO `Agent` (`Id_Agent`, `Id_Employe`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `Client`
--

CREATE TABLE `Client` (
  `Id_Client` int NOT NULL,
  `Prenom` varchar(50) NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Date_de_naissance` date NOT NULL,
  `Date_creation` date NOT NULL,
  `Profession` varchar(50) NOT NULL,
  `Num_tel` varchar(50) NOT NULL,
  `Mail` varchar(100) NOT NULL,
  `Adresse` varchar(50) NOT NULL,
  `Situation_familliale` varchar(50) DEFAULT NULL,
  `Id_Conseiller` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Client`
--

INSERT INTO `Client` (`Id_Client`, `Prenom`, `Nom`, `Date_de_naissance`, `Date_creation`, `Profession`, `Num_tel`, `Mail`, `Adresse`, `Situation_familliale`, `Id_Conseiller`) VALUES
(1, 'Josette', 'Du Potier', '1990-05-16', '2024-03-01', 'Développeur logiciel', '123456789', 'emily.taylor@email.com', '36, chemin Anne Roche 96700 Fernandes', 'Marrié', 1),
(2, 'Benoît', 'Blondel', '2002-08-24', '2024-03-05', 'Designer graphique', '987654321', 'michael.brown@email.com', '205, rue de Auger 22300 BruneauVille', 'Célibataire', 2);

-- --------------------------------------------------------

--
-- Structure de la table `Compte`
--

CREATE TABLE `Compte` (
  `Id_Compte` int NOT NULL,
  `Date_ouverture` datetime NOT NULL,
  `Date_fermeture` datetime DEFAULT NULL,
  `Solde` decimal(10,2) NOT NULL,
  `Decouvert` decimal(10,2) NOT NULL,
  `Id_Type_compte` int NOT NULL,
  `Id_Client` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Compte`
--

INSERT INTO `Compte` (`Id_Compte`, `Date_ouverture`, `Date_fermeture`, `Solde`, `Decouvert`, `Id_Type_compte`, `Id_Client`) VALUES
(1, '2024-03-01 09:00:00', NULL, 1000.00, 100.00, 1, 1),
(2, '2024-03-05 09:30:00', NULL, 500.00, 0.00, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Concerne`
--

CREATE TABLE `Concerne` (
  `Id_Rendezvous` int NOT NULL,
  `Id_Motif` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Concerne`
--

INSERT INTO `Concerne` (`Id_Rendezvous`, `Id_Motif`) VALUES
(1, 2),
(2, 5);

-- --------------------------------------------------------

--
-- Structure de la table `Conseiller`
--

CREATE TABLE `Conseiller` (
  `Id_Conseiller` int NOT NULL,
  `Id_Employe` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Conseiller`
--

INSERT INTO `Conseiller` (`Id_Conseiller`, `Id_Employe`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Contrat`
--

CREATE TABLE `Contrat` (
  `Id_Contrat` int NOT NULL,
  `Date_ouverture` datetime NOT NULL,
  `Date_fermeture` datetime DEFAULT NULL,
  `Libelle` varchar(50) NOT NULL,
  `Tarif_mensuel` decimal(10,2) NOT NULL,
  `Id_Type_contrat` int NOT NULL,
  `Id_Client` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Contrat`
--

INSERT INTO `Contrat` (`Id_Contrat`, `Date_ouverture`, `Date_fermeture`, `Libelle`, `Tarif_mensuel`, `Id_Type_contrat`, `Id_Client`) VALUES
(1, '2024-03-01 10:00:00', NULL, 'Assurance habitation', 15.00, 1, 1),
(2, '2024-03-05 11:00:00', NULL, 'Pret pour sa voiture', 25.00, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Directeur`
--

CREATE TABLE `Directeur` (
  `Id_Directeur` int NOT NULL,
  `Id_Employe` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Directeur`
--

INSERT INTO `Directeur` (`Id_Directeur`, `Id_Employe`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `Employe`
--

CREATE TABLE `Employe` (
  `Id_Employe` int NOT NULL,
  `Login` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Prenom` varchar(50) NOT NULL,
  `Nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Employe`
--

INSERT INTO `Employe` (`Id_Employe`, `Login`, `Password`, `Prenom`, `Nom`) VALUES
(1, 'jdoe', 'password123', 'John', 'Doe'),
(2, 'asmith', 'password123', 'Anna', 'Smith'),
(3, 'rjones', 'password123', 'Robert', 'Jones');

-- --------------------------------------------------------

--
-- Structure de la table `Historique_conseil`
--

CREATE TABLE `Historique_conseil` (
  `Id_Conseiller` int NOT NULL,
  `Id_Client` int NOT NULL,
  `Date_debut` datetime NOT NULL,
  `Date_fin` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Historique_conseil`
--

INSERT INTO `Historique_conseil` (`Id_Conseiller`, `Id_Client`, `Date_debut`, `Date_fin`) VALUES
(1, 1, '2024-03-01 09:00:00', NULL),
(2, 2, '2024-03-05 09:30:00', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Motif`
--

CREATE TABLE `Motif` (
  `Id_Motif` int NOT NULL,
  `Libelle` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Motif`
--

INSERT INTO `Motif` (`Id_Motif`, `Libelle`) VALUES
(1, NULL),
(2, NULL),
(3, NULL),
(4, NULL),
(5, 'Autre');

-- --------------------------------------------------------

--
-- Structure de la table `Necessite`
--

CREATE TABLE `Necessite` (
  `Id_Type_contrat` int NOT NULL,
  `Id_Piece_a_fournir` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Necessite`
--

INSERT INTO `Necessite` (`Id_Type_contrat`, `Id_Piece_a_fournir`) VALUES
(1, 1),
(2, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Necessite_1`
--

CREATE TABLE `Necessite_1` (
  `Id_Type_compte` int NOT NULL,
  `Id_Piece_a_fournir` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Necessite_1`
--

INSERT INTO `Necessite_1` (`Id_Type_compte`, `Id_Piece_a_fournir`) VALUES
(1, 1),
(2, 1),
(1, 3),
(2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `Operation`
--

CREATE TABLE `Operation` (
  `Num_operation` int NOT NULL,
  `Type_operation` varchar(50) NOT NULL,
  `Montant` decimal(10,2) NOT NULL,
  `Id_Compte` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Piece_a_fournir`
--

CREATE TABLE `Piece_a_fournir` (
  `Id_Piece_a_fournir` int NOT NULL,
  `Nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Piece_a_fournir`
--

INSERT INTO `Piece_a_fournir` (`Id_Piece_a_fournir`, `Nom`) VALUES
(1, 'Carte d_identite'),
(3, 'Etat civil'),
(4, 'Extrait de naissance'),
(2, 'Justificatif de domicile');

-- --------------------------------------------------------

--
-- Structure de la table `Rendez_vous`
--

CREATE TABLE `Rendez_vous` (
  `Id_Rendezvous` int NOT NULL,
  `Date_debut` datetime NOT NULL,
  `Date_fin` datetime NOT NULL,
  `Approuve_client` tinyint(1) NOT NULL,
  `Id_Conseiller` int NOT NULL,
  `Id_Agent` int NOT NULL,
  `Id_Client` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Rendez_vous`
--

INSERT INTO `Rendez_vous` (`Id_Rendezvous`, `Date_debut`, `Date_fin`, `Approuve_client`, `Id_Conseiller`, `Id_Agent`, `Id_Client`) VALUES
(1, '2024-03-15 09:00:00', '2024-03-15 10:00:00', 1, 1, 1, 1),
(2, '2024-06-20 14:00:00', '2024-06-20 15:00:00', 1, 2, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Tache_administrative`
--

CREATE TABLE `Tache_administrative` (
  `Id_Tache_administrative` int NOT NULL,
  `Date_debut` datetime NOT NULL,
  `Date_fin` datetime NOT NULL,
  `Libelle` varchar(50) NOT NULL,
  `Id_Conseiller` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Tache_administrative`
--

INSERT INTO `Tache_administrative` (`Id_Tache_administrative`, `Date_debut`, `Date_fin`, `Libelle`, `Id_Conseiller`) VALUES
(1, '2024-03-01 08:00:00', '2024-03-01 12:00:00', 'Formation', 1),
(2, '2024-06-01 09:00:00', '2024-06-01 11:00:00', 'Traitement de dossiers', 2);

-- --------------------------------------------------------

--
-- Structure de la table `Type_compte`
--

CREATE TABLE `Type_compte` (
  `Id_Type_compte` int NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Id_Motif` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Type_compte`
--

INSERT INTO `Type_compte` (`Id_Type_compte`, `Nom`, `Id_Motif`) VALUES
(1, 'Livret A', 1),
(2, 'Livret Jeune', 2);

-- --------------------------------------------------------

--
-- Structure de la table `Type_contrat`
--

CREATE TABLE `Type_contrat` (
  `Id_Type_contrat` int NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Id_Motif` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `Type_contrat`
--

INSERT INTO `Type_contrat` (`Id_Type_contrat`, `Nom`, `Id_Motif`) VALUES
(1, 'Assurance Habitation', 3),
(2, 'Pret a la consomation', 4);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Agent`
--
ALTER TABLE `Agent`
  ADD PRIMARY KEY (`Id_Agent`),
  ADD UNIQUE KEY `Id_Employe` (`Id_Employe`);

--
-- Index pour la table `Client`
--
ALTER TABLE `Client`
  ADD PRIMARY KEY (`Id_Client`),
  ADD UNIQUE KEY `Mail` (`Mail`),
  ADD KEY `Id_Conseiller` (`Id_Conseiller`);

--
-- Index pour la table `Compte`
--
ALTER TABLE `Compte`
  ADD PRIMARY KEY (`Id_Compte`),
  ADD KEY `Id_Type_compte` (`Id_Type_compte`),
  ADD KEY `Id_Client` (`Id_Client`);

--
-- Index pour la table `Concerne`
--
ALTER TABLE `Concerne`
  ADD PRIMARY KEY (`Id_Rendezvous`,`Id_Motif`),
  ADD KEY `Id_Motif` (`Id_Motif`);

--
-- Index pour la table `Conseiller`
--
ALTER TABLE `Conseiller`
  ADD PRIMARY KEY (`Id_Conseiller`),
  ADD UNIQUE KEY `Id_Employe` (`Id_Employe`);

--
-- Index pour la table `Contrat`
--
ALTER TABLE `Contrat`
  ADD PRIMARY KEY (`Id_Contrat`),
  ADD KEY `Id_Type_contrat` (`Id_Type_contrat`),
  ADD KEY `Id_Client` (`Id_Client`);

--
-- Index pour la table `Directeur`
--
ALTER TABLE `Directeur`
  ADD PRIMARY KEY (`Id_Directeur`),
  ADD UNIQUE KEY `Id_Employe` (`Id_Employe`);

--
-- Index pour la table `Employe`
--
ALTER TABLE `Employe`
  ADD PRIMARY KEY (`Id_Employe`),
  ADD UNIQUE KEY `Login` (`Login`);

--
-- Index pour la table `Historique_conseil`
--
ALTER TABLE `Historique_conseil`
  ADD PRIMARY KEY (`Id_Conseiller`,`Id_Client`),
  ADD KEY `Id_Client` (`Id_Client`);

--
-- Index pour la table `Motif`
--
ALTER TABLE `Motif`
  ADD PRIMARY KEY (`Id_Motif`);

--
-- Index pour la table `Necessite`
--
ALTER TABLE `Necessite`
  ADD PRIMARY KEY (`Id_Type_contrat`,`Id_Piece_a_fournir`),
  ADD KEY `Id_Piece_a_fournir` (`Id_Piece_a_fournir`);

--
-- Index pour la table `Necessite_1`
--
ALTER TABLE `Necessite_1`
  ADD PRIMARY KEY (`Id_Type_compte`,`Id_Piece_a_fournir`),
  ADD KEY `Id_Piece_a_fournir` (`Id_Piece_a_fournir`);

--
-- Index pour la table `Operation`
--
ALTER TABLE `Operation`
  ADD PRIMARY KEY (`Num_operation`),
  ADD KEY `Id_Compte` (`Id_Compte`);

--
-- Index pour la table `Piece_a_fournir`
--
ALTER TABLE `Piece_a_fournir`
  ADD PRIMARY KEY (`Id_Piece_a_fournir`),
  ADD UNIQUE KEY `Nom` (`Nom`);

--
-- Index pour la table `Rendez_vous`
--
ALTER TABLE `Rendez_vous`
  ADD PRIMARY KEY (`Id_Rendezvous`),
  ADD KEY `Id_Conseiller` (`Id_Conseiller`),
  ADD KEY `Id_Agent` (`Id_Agent`),
  ADD KEY `Id_Client` (`Id_Client`);

--
-- Index pour la table `Tache_administrative`
--
ALTER TABLE `Tache_administrative`
  ADD PRIMARY KEY (`Id_Tache_administrative`),
  ADD KEY `Id_Conseiller` (`Id_Conseiller`);

--
-- Index pour la table `Type_compte`
--
ALTER TABLE `Type_compte`
  ADD PRIMARY KEY (`Id_Type_compte`),
  ADD UNIQUE KEY `Nom` (`Nom`),
  ADD KEY `Id_Motif` (`Id_Motif`);

--
-- Index pour la table `Type_contrat`
--
ALTER TABLE `Type_contrat`
  ADD PRIMARY KEY (`Id_Type_contrat`),
  ADD UNIQUE KEY `Nom` (`Nom`),
  ADD KEY `Id_Motif` (`Id_Motif`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Agent`
--
ALTER TABLE `Agent`
  MODIFY `Id_Agent` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `Client`
--
ALTER TABLE `Client`
  MODIFY `Id_Client` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Compte`
--
ALTER TABLE `Compte`
  MODIFY `Id_Compte` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Conseiller`
--
ALTER TABLE `Conseiller`
  MODIFY `Id_Conseiller` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Contrat`
--
ALTER TABLE `Contrat`
  MODIFY `Id_Contrat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Directeur`
--
ALTER TABLE `Directeur`
  MODIFY `Id_Directeur` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `Employe`
--
ALTER TABLE `Employe`
  MODIFY `Id_Employe` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `Motif`
--
ALTER TABLE `Motif`
  MODIFY `Id_Motif` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `Operation`
--
ALTER TABLE `Operation`
  MODIFY `Num_operation` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `Piece_a_fournir`
--
ALTER TABLE `Piece_a_fournir`
  MODIFY `Id_Piece_a_fournir` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `Rendez_vous`
--
ALTER TABLE `Rendez_vous`
  MODIFY `Id_Rendezvous` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Tache_administrative`
--
ALTER TABLE `Tache_administrative`
  MODIFY `Id_Tache_administrative` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Type_compte`
--
ALTER TABLE `Type_compte`
  MODIFY `Id_Type_compte` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Type_contrat`
--
ALTER TABLE `Type_contrat`
  MODIFY `Id_Type_contrat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `Agent`
--
ALTER TABLE `Agent`
  ADD CONSTRAINT `Agent_ibfk_1` FOREIGN KEY (`Id_Employe`) REFERENCES `Employe` (`Id_Employe`);

--
-- Contraintes pour la table `Client`
--
ALTER TABLE `Client`
  ADD CONSTRAINT `Client_ibfk_1` FOREIGN KEY (`Id_Conseiller`) REFERENCES `Conseiller` (`Id_Conseiller`);

--
-- Contraintes pour la table `Compte`
--
ALTER TABLE `Compte`
  ADD CONSTRAINT `Compte_ibfk_1` FOREIGN KEY (`Id_Type_compte`) REFERENCES `Type_compte` (`Id_Type_compte`),
  ADD CONSTRAINT `Compte_ibfk_2` FOREIGN KEY (`Id_Client`) REFERENCES `Client` (`Id_Client`);

--
-- Contraintes pour la table `Concerne`
--
ALTER TABLE `Concerne`
  ADD CONSTRAINT `Concerne_ibfk_1` FOREIGN KEY (`Id_Rendezvous`) REFERENCES `Rendez_vous` (`Id_Rendezvous`),
  ADD CONSTRAINT `Concerne_ibfk_2` FOREIGN KEY (`Id_Motif`) REFERENCES `Motif` (`Id_Motif`);

--
-- Contraintes pour la table `Conseiller`
--
ALTER TABLE `Conseiller`
  ADD CONSTRAINT `Conseiller_ibfk_1` FOREIGN KEY (`Id_Employe`) REFERENCES `Employe` (`Id_Employe`);

--
-- Contraintes pour la table `Contrat`
--
ALTER TABLE `Contrat`
  ADD CONSTRAINT `Contrat_ibfk_1` FOREIGN KEY (`Id_Type_contrat`) REFERENCES `Type_contrat` (`Id_Type_contrat`),
  ADD CONSTRAINT `Contrat_ibfk_2` FOREIGN KEY (`Id_Client`) REFERENCES `Client` (`Id_Client`);

--
-- Contraintes pour la table `Directeur`
--
ALTER TABLE `Directeur`
  ADD CONSTRAINT `Directeur_ibfk_1` FOREIGN KEY (`Id_Employe`) REFERENCES `Employe` (`Id_Employe`);

--
-- Contraintes pour la table `Historique_conseil`
--
ALTER TABLE `Historique_conseil`
  ADD CONSTRAINT `Historique_conseil_ibfk_1` FOREIGN KEY (`Id_Conseiller`) REFERENCES `Conseiller` (`Id_Conseiller`),
  ADD CONSTRAINT `Historique_conseil_ibfk_2` FOREIGN KEY (`Id_Client`) REFERENCES `Client` (`Id_Client`);

--
-- Contraintes pour la table `Necessite`
--
ALTER TABLE `Necessite`
  ADD CONSTRAINT `Necessite_ibfk_1` FOREIGN KEY (`Id_Type_contrat`) REFERENCES `Type_contrat` (`Id_Type_contrat`),
  ADD CONSTRAINT `Necessite_ibfk_2` FOREIGN KEY (`Id_Piece_a_fournir`) REFERENCES `Piece_a_fournir` (`Id_Piece_a_fournir`);

--
-- Contraintes pour la table `Necessite_1`
--
ALTER TABLE `Necessite_1`
  ADD CONSTRAINT `Necessite_1_ibfk_1` FOREIGN KEY (`Id_Type_compte`) REFERENCES `Type_compte` (`Id_Type_compte`),
  ADD CONSTRAINT `Necessite_1_ibfk_2` FOREIGN KEY (`Id_Piece_a_fournir`) REFERENCES `Piece_a_fournir` (`Id_Piece_a_fournir`);

--
-- Contraintes pour la table `Operation`
--
ALTER TABLE `Operation`
  ADD CONSTRAINT `Operation_ibfk_1` FOREIGN KEY (`Id_Compte`) REFERENCES `Compte` (`Id_Compte`);

--
-- Contraintes pour la table `Rendez_vous`
--
ALTER TABLE `Rendez_vous`
  ADD CONSTRAINT `Rendez_vous_ibfk_1` FOREIGN KEY (`Id_Conseiller`) REFERENCES `Conseiller` (`Id_Conseiller`),
  ADD CONSTRAINT `Rendez_vous_ibfk_2` FOREIGN KEY (`Id_Agent`) REFERENCES `Agent` (`Id_Agent`),
  ADD CONSTRAINT `Rendez_vous_ibfk_3` FOREIGN KEY (`Id_Client`) REFERENCES `Client` (`Id_Client`);

--
-- Contraintes pour la table `Tache_administrative`
--
ALTER TABLE `Tache_administrative`
  ADD CONSTRAINT `Tache_administrative_ibfk_1` FOREIGN KEY (`Id_Conseiller`) REFERENCES `Conseiller` (`Id_Conseiller`);

--
-- Contraintes pour la table `Type_compte`
--
ALTER TABLE `Type_compte`
  ADD CONSTRAINT `Type_compte_ibfk_1` FOREIGN KEY (`Id_Motif`) REFERENCES `Motif` (`Id_Motif`);

--
-- Contraintes pour la table `Type_contrat`
--
ALTER TABLE `Type_contrat`
  ADD CONSTRAINT `Type_contrat_ibfk_1` FOREIGN KEY (`Id_Motif`) REFERENCES `Motif` (`Id_Motif`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
